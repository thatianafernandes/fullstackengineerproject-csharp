using System;
using System.Linq;
using Application.Services;
using Xunit;

namespace Application.UnitTests
{
    public class PaginationServiceTests
    {
        [Fact]
        public void DontAskForPages()
        {
            var list = AlarmRepositoryFixture.createAlarms().getList();
            var service = new PaginationService();
            var result = service.GetResultPage(list, null, null);
            Assert.Equal(list.Count, result.ToList().Count);            
        }   

        [Theory]
        [InlineData(3, 1, 3)]
        [InlineData(3, 2, 2)]
        [InlineData(2, 2, 2)]
        [InlineData(2, 3, 1)]
        [InlineData(8, 1, 5)]
        public void FilterPages(int? pageSize, int? pageNumber, int expectedNumberOfResults)
        {
            var list = AlarmRepositoryFixture.createAlarms().getList();
            var service = new PaginationService();
            var result = service.GetResultPage(list, pageSize, pageNumber);
            Assert.Equal(expectedNumberOfResults, result.ToList().Count);            
        }           

        
        [Fact]
        public void FilterPagesWithException()
        {
            const int pageSize = 10;
            const int pageNumber = 2;
            var list = AlarmRepositoryFixture.createAlarms().getList();
            var service = new PaginationService();
            Assert.Throws<ArgumentException>(() => service.GetResultPage(list, pageSize, pageNumber));
        }     
    }


}