using System;
using Xunit;
using Application.Services;
using Infrastructure.Repository;
using Moq;

namespace Application.UnitTests
{
    public class AlarmServiceTests
    {

        private readonly Mock<IAlarmRepository> _repo;

        public AlarmServiceTests() {
            _repo = AlarmRepositoryFixture.GetMockWith5Alarms();            
        }
       

        [Theory]
        [InlineData(true, 3)]
        [InlineData(false, 2)]
        public void FilterOutcome(bool filteredOutcome, int numberOfAlarms)
        {
            var service = new AlarmService(_repo.Object);
            var list = service.Filter(filteredOutcome, null, null, null);
            Assert.Equal(numberOfAlarms, list.getList().Count);            
        }

        [Fact]
        public void FilterNone()
        {
            var service = new AlarmService(_repo.Object);
            var list = service.Filter(null, null, null, null);
            Assert.Equal(5, list.getList().Count);            
        }


        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 3)]
        [InlineData(3, 0)]
        [InlineData(4, 1)]
        public void FilterLocation(int locationNumber, int numberOfAlarms)
        {
            var service = new AlarmService(_repo.Object);
            var list = service.Filter(null, null, null, locationNumber);
            Assert.Equal(numberOfAlarms, list.getList().Count);            
        }     

        [Fact]
        public void FilterStartDateTime()
        {
            var service = new AlarmService(_repo.Object);
            var d = DateTime.Today.AddDays(-1);            
            var list = service.Filter(null, d, null, null);
            Assert.Equal(4, list.getList().Count);     
        }   


        [Fact]
        public void FilterEndDateTime()
        {
            var service = new AlarmService(_repo.Object);
            var d = DateTime.Today.AddHours(10);
            
            var list = service.Filter(null, null, d, null);
            Assert.Equal(2, list.getList().Count);            
        }               


        [Fact]
        public void FilterStartAndEndDateTime()
        {
            var service = new AlarmService(_repo.Object);
            var start = DateTime.Today;
            var end = DateTime.Today.AddHours(14);

            var list = service.Filter(null, start, end, null);
            Assert.Equal(2, list.getList().Count);            
        }           

        [Fact]
        public void FilterStartLaterThanEnd()
        {
            var service = new AlarmService(_repo.Object);
            var end = DateTime.Today;
            var start = DateTime.Today.AddHours(14);

            var list = service.Filter(null, start, end, null);
            Assert.Equal(0, list.getList().Count);            
        }               

        [Fact]
        public void FilterLocationAndOutcome()
        {
            var service = new AlarmService(_repo.Object);
            var list = service.Filter(false, null, null, 1);
            Assert.Equal(1, list.getList().Count);            
        }        

    }
}
