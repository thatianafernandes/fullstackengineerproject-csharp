using System;
using System.Collections.Generic;
using Infrastructure.Model;
using Infrastructure.Repository;
using Moq;

namespace Application.UnitTests
{
    public class AlarmRepositoryFixture {

        public static Mock<IAlarmRepository> GetMockWith5Alarms() {
            Mock<IAlarmRepository> repo = new Mock<IAlarmRepository>();
            repo.Setup(x => x.GetListOfAlarms()).Returns(createAlarms());
            return repo;
        }


        private static Alarm createAlarm(bool outcome, DateTime date, int location) {
            var alarm = new Alarm();
            alarm.Outcome = outcome;
            alarm.Timestamp = date;
            alarm.Location = location;
            return alarm;
        }
        public static ListAlarm createAlarms() {
            return new ListAlarm(new List<Alarm>() {
                createAlarm(true, DateTime.Today.AddHours(12), 0),
                createAlarm(true, DateTime.Today.AddHours(5), 1),
                createAlarm(false, DateTime.Today.AddDays(1), 1),
                createAlarm(true, DateTime.Today.AddDays(-2), 1),
                createAlarm(false, DateTime.Today.AddHours(15), 4)                
            });
        }        
    }        

    

}