using Infrastructure.DataReader;
using Infrastructure.Repository;
using Xunit;

namespace Infrastructure.UnitTests
{

     public class AlarmRepositoryTests
    {
        [Fact]
        public void TestReadLocation()
        {
            var AlarmRepository = new AlarmsRepository();
            Assert.All(AlarmRepository.GetListOfAlarms().getList(), alarm => Assert.NotNull(alarm.LocationName));                     
        }

    }
}
