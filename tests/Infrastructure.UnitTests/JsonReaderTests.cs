using Infrastructure.DataReader;
using Xunit;

namespace Infrastructure.UnitTests
{

     public class JsonReaderTests
    {
        [Fact]
        public void ReadAlarms()
        {
            var alarms = JsonReader.ReadData().Alarms;  
            Assert.Equal(100000, alarms.Count);                      
        }


        [Fact]
        public void ReadLocations()
        {
            var locations = JsonReader.ReadData().Locations;
            Assert.Equal(100, locations.Count);
        }
    }
}
