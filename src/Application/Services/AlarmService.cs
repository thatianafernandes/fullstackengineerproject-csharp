﻿using System;
using Infrastructure.Repository;
using Infrastructure.Model;


namespace Application.Services
{
public class AlarmService : IAlarmService {

        private readonly IAlarmRepository _repository;

        public AlarmService(IAlarmRepository repository)
        {
            _repository = repository;
        }

        public ListAlarm GetAllAlarms() {
            return _repository.GetListOfAlarms();
        }


        public ListAlarm Filter(bool? outcome, DateTime? start, DateTime? end, int? location)
        {
            return GetAllAlarms()
            .FilterOutcome(outcome)
            .FilterDateTime(start, end)
            .FilterLocation(location);
        }

    }
}
