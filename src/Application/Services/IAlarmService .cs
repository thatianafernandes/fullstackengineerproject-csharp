﻿using System;
using Infrastructure.Model;

namespace Application.Services
{
public interface IAlarmService {
    
        ListAlarm Filter(bool? outcome, DateTime? start, DateTime? end, int? location);
        ListAlarm GetAllAlarms();
    }
}
