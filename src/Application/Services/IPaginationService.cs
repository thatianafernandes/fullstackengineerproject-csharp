using System;
using System.Collections.Generic;
using Infrastructure.Model;

namespace Application.Services {
    public interface IPaginationService
    {
        IEnumerable<Alarm> GetResultPage(IList<Alarm> alarms, int? parameterPageSize, int? parameterPageNumber);
    }

}