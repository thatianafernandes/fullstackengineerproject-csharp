using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.Model;

namespace Application.Services {

    public class PaginationService : IPaginationService
    {

        public IEnumerable<Alarm> GetResultPage(IList<Alarm> alarms, int? parameterPageSize, int? parameterPageNumber)
        {
            int pageSize = parameterPageSize ?? alarms.Count;

            long pageNumber = parameterPageNumber ?? 1;
            long numberOfPages = (alarms.Count <= pageSize) ? 1 : GetNumberOfPages(alarms, pageSize);

            if (pageNumber > numberOfPages || pageNumber < 1)
            {
                throw new ArgumentException("page not found");
            }

            return GetAlarmsOfPage(alarms, pageSize, pageNumber);
        }

        private IEnumerable<Alarm> GetAlarmsOfPage(IList<Alarm> alarms, long pageSize, long pageNumber)
        {
            long rangeInit = (pageNumber - 1) * (pageSize);

            return alarms.Skip((int) rangeInit).Take((int) pageSize).ToList();

        }

        private int GetNumberOfPages(IList<Alarm> alarms, int pageSize)
        {
            return ((alarms.Count / pageSize)) + ((alarms.Count % pageSize > 0) ? 1 : 0);
        }
    }

}