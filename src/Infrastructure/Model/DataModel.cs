using System.Collections.Generic;
using Infrastructure.DataReader;

namespace Infrastructure.Model 
{
    public class DataModel 
    {
        public IList<AlarmVO> Alarms { get; set; }
        public IList<Location> Locations { get; set; }

    }
}