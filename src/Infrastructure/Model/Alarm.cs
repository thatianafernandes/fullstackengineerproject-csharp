using System;


namespace Infrastructure.Model
{
    public class Alarm
    {
        public bool Outcome { get; set; }

        public long Location { get; set; }

        public string LocationName { get; set; }

        public DateTime Timestamp { get; set; }
    }
}