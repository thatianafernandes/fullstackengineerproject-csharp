using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Model
{
    public class ListAlarm 
    {
        private IEnumerable<Alarm> _list;

        public ListAlarm() {
            this._list = new List<Alarm>();
        }

        public ListAlarm(IEnumerable<Alarm> list) {
            this._list = list.OrderBy(x => x.Timestamp).ThenBy(x => x.Location);
        }

        public ListAlarm FilterOutcome(bool? outcome) {
            if (outcome is null) {
                return this;
            }

            return new ListAlarm(_list.Where(x => x.Outcome == outcome));
        }

        public ListAlarm FilterDateTime(DateTime? start, DateTime? end) {
            var list =  _list;

            if (start is not null) {
                list = list.Where(x => x.Timestamp >= start);
            }

            if (end is not null) {
                list = list.Where(x => x.Timestamp <= end);
            }

            return new ListAlarm(list);
        }

        public ListAlarm FilterLocation(int? location) {
            if (location is null) {
                return this;
            }
             return new ListAlarm(_list.Where(x => x.Location == location));
        }

        public IList<Alarm> getList() {
            return _list.ToList();
        }

    }

}