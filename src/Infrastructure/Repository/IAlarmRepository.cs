using System.Collections.Generic;
using Infrastructure.Model;

namespace Infrastructure.Repository {

    public interface IAlarmRepository
    {
        ListAlarm GetListOfAlarms();
        IList<Location> GetListOfLocations();
    }

}