using System.Collections.Generic;
using System.Linq;
using Infrastructure.DataReader;
using Infrastructure.Model;

namespace Infrastructure.Repository {


    public class AlarmsRepository : IAlarmRepository
    {
        private  IList<Alarm> _alarms;
        private IList<Location> _locations;

        public AlarmsRepository()
        {
            var dataModel = JsonReader.ReadData();
            _locations = dataModel.Locations;
            MapAlarmLocation(dataModel);
        }

        private void MapAlarmLocation(DataModel dataModel) {
            this._alarms = new List<Alarm>();
            foreach (var alarmVo in dataModel.Alarms)
            {                
                var locationName = dataModel.Locations.Single(x => x.Id ==  alarmVo.Location).Name;  
                var alarm = Mapping.Mapper.Map<Alarm>(alarmVo);
                alarm.LocationName = locationName; 
                _alarms.Add(alarm);
            }
        }
            


        public ListAlarm GetListOfAlarms()
        {
            return new ListAlarm(this._alarms);
        }

        public IList<Location> GetListOfLocations() => this._locations;
    }
}