using System;
using Infrastructure.Model;
using AutoMapper;

namespace Infrastructure.DataReader
{
    public class AlarmVO {
        public bool Outcome { get; set; }

        public long Location { get; set; }

        public DateTime Timestamp { get; set; }

    }
}