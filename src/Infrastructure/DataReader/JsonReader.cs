﻿using Infrastructure.Model;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

using System.Linq;

namespace Infrastructure.DataReader
{
     public class JsonReader 
    {
        private static readonly string resourceEndName = "data.json";

        public JsonReader() {
        }

        private static string GetResourceName(Assembly assembly) {
            return assembly.GetManifestResourceNames().Single(name => name.EndsWith(resourceEndName));
        }
        
        private static string ReadResourceFile() {
            var assembly = Assembly.GetExecutingAssembly();

            using (Stream stream = assembly.GetManifestResourceStream(GetResourceName(assembly))) {
                using (StreamReader reader = new StreamReader(stream))
                {
                   return reader.ReadToEnd();
                }
            }

        }

        public static DataModel ReadData()
        {       
            var settings = new JsonSerializerSettings
            {
                DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind,
            };            
            
            var dataModel = JsonConvert.DeserializeObject<DataModel>(ReadResourceFile(), settings);
            foreach (var location in dataModel.Locations)
            {
                location.Name = location.Name.Remove(location.Name.Length - 1);
            }

            return dataModel;
        }

    }
}
