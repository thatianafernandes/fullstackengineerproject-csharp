using System;

namespace Endpoints.Controllers 
{
        public class AlarmsQueryData
    {
        public bool? Outcome { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End{ get; set; }

        public int? PageSize {get; set;} 

        public int? PageNumber {get; set;} 

        public int? Location { get; set; }

        public string toString() {
            string strParams = String.Empty;
            strParams += getParam(Outcome, "outcome");
            strParams += getDateParam(Start, "start");
            strParams += getDateParam(End, "end");
            strParams += getParam(Location, "location");
            return strParams;
        }

        private string getParam(Object param, string name) {
            return (param is not null)? $"{name}:{param}; ": String.Empty;
        }

        private string getDateParam(DateTime? param, string name) {
            return (param.HasValue)? $"{name}:" + param.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "; ": String.Empty;
        }        
        
    }
}