﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Application.Services;
using Infrastructure.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Endpoints.Controllers
{
    [ApiController]
  //  [Authorize]    
    [Route("[controller]")]
    public class AlarmsController : ControllerBase
    {
        private readonly ILogger<AlarmsController> _logger;
        private readonly IAlarmService _service;

        private readonly IPaginationService _paginationService;

        public AlarmsController(ILogger<AlarmsController> logger, IAlarmService alarmService, IPaginationService paginationService)
        {
            _logger = logger;
            _service = alarmService;
            _paginationService = paginationService;
        }

        [HttpGet]
        [Route("/all")]
        public IEnumerable<Alarm> Get()
        {
            _logger.LogInformation("endPoint: Alarms/all");
            return _service.GetAllAlarms().getList();
        }


        [HttpGet]
        public ActionResult<IEnumerable<Alarm>> QueryData([FromQuery] AlarmsQueryData queryData)
        {            
            LogData(queryData);

            var list = _service.Filter(queryData.Outcome, 
                    queryData.Start, 
                    queryData.End, 
                    queryData.Location).getList();

            try {
                return Ok(_paginationService.GetResultPage(list, queryData.PageSize, queryData.PageNumber));
            }
            catch (ArgumentException ex) {
                return BadRequest(ex.Message);
            }
            catch(Exception ex) {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        } 

        
        private void LogData(AlarmsQueryData queryData) {
            var queryDataStr = (queryData is not null)? queryData.toString() : "";
            _logger.LogInformation("endPoint: QueryData; " + queryDataStr);
        }  
    }

}
