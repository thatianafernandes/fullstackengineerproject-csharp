# Alarms
This project is an API that allows the user to search and filter a set of alarm events.

An alarm event is composed by the following fields:
```sh
    		{
		    		"outcome": an boolean
		    		"timestamp": an ISO 8601 timestamp
		    		"location": an integer representing the location
		    		"location name": the name of the location
			}
```

## Running the project locally (command line)
* #### Backend
```sh
dotnet run src/Endpoints/Endpoints.csproj
```
* #### Frontend
```sh
npm run serve
``` 

Backend will start on *localhost:5000* and frontend will start on *localhost:8080*.

# Endpoints

* "/alarms/all" : Gets all the alarms. Returns a lot of data. Use carefully.
* "/alarms": allow filtering with query parameters:
  * outcome: filter by the outcome of the event. Two values are possible: *true* or *false*
  * start: filter all the alarms that have the timestamp equal or after the timestamp given as this parameter. It is expected the timestamp to be in the ISO 8601 format (for exemple: ''2021-01-02T00:00:26.363Z``).
  * end: filter all the alarms that have the timestamp equal or before the timestamp given as this parameter. Same as start, it should be in ISO 8601 format.
  * location: filter by the number of the location.
  * pageNumber: Limit the number of alarms, paginating the results.
  * pageSize: Controls which page of results will be fetched. 
  
## Tech
    
Backend was developed using C#/.Net Framework 5. In the solution there are 5 projects: 
* Enpoints: basically controllers;
* Application: use cases modeled as services;
* Infrastrucutre: Model, Repositories, json readers;

Model was too small to have its own project, so it was added to the Infrastructure.

When the application starts, it loads all data in a big list in memory. This could be improved persinting the alarms in a database.

Frontend was developed in vue.js. Few components for the form and a expansible list showing the results. The design and error handling could be improved, but I had no time to do that :(
